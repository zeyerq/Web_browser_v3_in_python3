# Web_browser_v3_in_python3

Chromium-based web browser that uses the PySide6 module. Imposing, robust, focused on protecting your privacy. Proxy support and by default all anti-privacy settings are disabled.