#!/usr/bin/env python3.11

import os
import sys
import subprocess

def start_php_server(host, port, search_path):
    if not os.path.exists(search_path):
        print(f"The directory '{search_path}' does not exist.")
        return

# Check if the required files exist in the directory
    required_files = ["index.php", "search_results.php", "styles.css"]
    missing_files = [file for file in required_files if not os.path.isfile(os.path.join(search_path, file))]
    if missing_files:
        print(f"Missing files: {', '.join(missing_files)}")
        return

# Start the PHP server
    command = f"php8.2 -S {host}:{port}"
    try:
        subprocess.run(command, cwd=search_path, shell=True, check=True)
    except subprocess.CalledProcessError as error:
        print(f"Error starting PHP server: {error}")

#if __name__ == "__main__":
#    try:
#        start_php_server("127.0.0.1", 42781)
#    except:
#        os.system("kill -9 $(lsof -t -i :42781)")
#        sys.exit()

#http://127.0.0.1:42781/index.php
