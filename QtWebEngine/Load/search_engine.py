#!/usr/bin/env python3.11

SEARCH_ENGINES = {
    'AOL': 'https://search.aol.com/aol/search?q=',
    'Bing': 'https://www.bing.com/search?q=',
    'Duckduckgo': 'https://duckduckgo.com/?t=ftsa&q=',
    'DuckDuckGo lite': 'https://html.duckduckgo.com/html?q=',
    'Ecosia': 'https://www.ecosia.org/search?method=index&q=',
    'Google': 'https://www.google.com/search?q=',
    'MetaGer': 'https://metager.org/meta/meta.ger3?eingabe=',
    'Mojeek': 'https://www.mojeek.com/search?q=',
    'Qwant': 'https://lite.qwant.com/?q=',
    'Search Encrypt': 'https://www.searchencrypt.com/search?q=',
    'Searx Nixnet': 'https://searx.nixnet.services/search?q=',
    'Searx Zapashcanon': 'https://searx.zapashcanon.fr/search?q=',
    'Yahoo': 'https://br.search.yahoo.com/search?p=',
    'Yandex': 'https://www.yandex.com/search/?text=',
}