#!/usr/bin/env python3.11

fixed = {
    'DuckDuckGo': 'https://duckduckgo.com',
    'DuckDuckGo html': 'https://html.duckduckgo.com/html',
    'Google': 'https://google.com',
    'YouTube': 'https://www.youtube.com/',
    'ChatGPT': 'https://openai.com/chatgpt',
}

bookmarks = {
    'Nginx download': 'https://nginx.org/download/',
    'Invidious': 'https://invidious.flokinet.to/feed/popular',
    'Translate projectsegfau': 'https://translate.projectsegfau.lt/',
    'DNS Leak': 'https://www.dnsleaktest.com/',
    'Fingerprint Information': 'https://www.deviceinfo.me/',
    'Kali Linux': 'https://www.kali.org/get-kali/#kali-platforms',
    'Debian Download': 'http://debian.c3sl.ufpr.br/debian-cd/',
}

onion = {
    'Respostas Ocultas': 'http://xh6liiypqffzwnu5734ucwps37tn2g6npthvugz3gdoqpikujju525yd.onion/',
    'DuckDuckGo Onion': 'https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion',
    'Tor Project Onion': 'http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion/index.html',
}

shortcuts = f'''
Shortcuts:
    ctrl+q                Close the browser
    ctrl+x                Close the browser
    ctrl+u                View page source
    ctrl+y                View page source in new window
    ctrl+z                Close page source in new window
    esc                   Close page source in new window
    ctrl+Key_Left         Back_shortcut  
    ctrl+Key_Right        Forward_shortcut
    ctrl+r                Rem tab shortcut
    ctrl+n                Add tab shortcut
    ctrl+s                Refresh shortcut
'''

user_agent = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
user_agent_ = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'}

